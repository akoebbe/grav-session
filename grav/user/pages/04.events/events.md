---
title: Events
body_classes: 'header-image fullwidth'
child_type: event
sitemap:
    changefreq: monthly
    priority: 1.03
content:
    items: '@self.children'
    order:
        by: header.event.time
        dir: desc
    limit: 5
    pagination: true
    dateRange:
        start: today
        field: header.event.time
feed:
    description: 'Jessica Koebbe Events'
    limit: 10
pagination: true
---

