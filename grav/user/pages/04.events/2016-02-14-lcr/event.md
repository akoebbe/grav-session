---
title: 'Fourth Annual Valentine''s Day Concert'
date: '02-01-2016 00:00'
event:
    time: '14-02-2016 19:30'
    venue_name: 'Lutheran Church of the Resurrection'
    venue_address: "9100 Mission Road\r\nPrairie Village, KS 66206"
    url: 'http://www.midwestchamberensemble.org/concerts/'
---

Midwest Chamber Ensemble's Chamber Music Coordinator, pianist Jessica Koebbe, leads Midwest Chamber Ensemble's annual Valentine's Day performance of Romantic chamber music.