---
title: 'Ladies'' Night 2.0'
date: '02-08-2015 00:00'
event:
    time: '11-10-2015 19:30'
    venue_name: 'Lutheran Church of the Resurrection'
    venue_address: "9100 Mission Road\r\nPrairie Village, KS 66206"
    url: 'http://www.midwestchamberensemble.org/concerts/'

---

Members of the Midwest Chamber Ensemble present chamber music by female composers, including Kansas City's own Ingrid Stölzel, assistant professor at the University of Kansas.